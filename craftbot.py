from crafty_client import Crafty4
from crafty_client.static import exceptions as crafty_exc
import discord
from discord.ext import commands
import json
import pprint
import socket
import sys
import tabulate
try:
    import jsmin
    jsmin_avail = True
except ImportError:
    jsmin_avail = False
    print("WARN: Could not load jsmin. Comments in the config file will not be supported.")

if len(sys.argv) > 1 and len(sys.argv[1]) > 0:
    config_filename = sys.argv[1]
else:
    config_filename = 'config.json'

with open(config_filename) as config_file:
    if jsmin_avail:
        config = json.loads(jsmin.jsmin(config_file.read()))
    else:
        config = json.load(config_file)

try:
    config['server_list_fields'] is None
except KeyError:
    config['server_list_fields'] = ['running', 'crashed', 'auto_start']
try:
    config['server_stat_fields'] is None
except KeyError:
    config['server_stat_fields'] = ['desc', 'version', 'running', 'started', 'crashed', '_online_max', 'cpu', 'mem', 'world_size']

cweb = Crafty4(config['crafty_url'], config['crafty_token'], verify_ssl=config['verify_ssl'])

intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix='$', intents=intents)

def make_embed(title):
    e = discord.Embed(
        title='{}'.format(title),
        color=0x00ff00
    )
    e.set_author(name='{}'.format(socket.gethostname()))
    return e

def crafty_control(action, id_str, *args):
    embed = make_embed("Server control")
    try:
        sv_id = int(id_str)
    except TypeError:
        embed.color = 0xff00ff
        embed.add_field(name='{} error'.format(action), value='{} is not a vaid integer'.format(id_str))
        return embed
    stats = cweb.get_server_stats(id_str)
    metadata = stats['server_id']
    status = False
    try:
        if action == 'start':
            status = cweb.start_server(sv_id)
        elif action == 'stop':
            status = cweb.stop_server(sv_id)
        elif action == 'restart':
            status = cweb.restart_server(sv_id)
        elif action == 'query':
            status = True
        elif action == 'command':
            status = cweb.run_command(sv_id, *args)
        elif action == 'backup':
            status = cweb.backup_server(sv_id)
        else:
            embed.color = 0xff00ff
            embed.add_field(title="command error", value="somehow, SOMEHOW, you got the action {} into an internal function.  You should be proud of yourself and report this to the developer".format(action))
    except crafty_exc.ServerNotFound:
        embed.color=0xff0000
        embed.add_field(name='{} {}'.format(action, metadata['server_name']),
                        value='ERROR: Server with ID {} not found!'.format(sv_id))
    except crafty_exc.ServerNotRunning:
        embed.color=0xff0000
        embed.add_field(name='{} {}'.format(action, metadata['server_name']),
                        value='ERROR: Server is not running')
    except crafty_exc.ServerAlreadyRunning:
        embed.color=0xff0000
        embed.add_field(name='{} {}'.format(action, metadata['server_name']),
                        value='ERROR: Server is already running')
    except crafty_exc.AccessDenied:
        embed.color=0xff00ff
        embed.add_field(name='{} {}'.format(action, metadata['server_name']),
                        value='ERROR: The bot account on Crafty Controller was denied access to this command')
    except crafty_exc.MissingParameters as e:
        embed.color=0xff00ff
        embed.add_field(name='{} {}'.format(action, metadata['server_name']),
                        value='ERROR: How did you even do this??? Missing parameters: {}'.format(e))
    except crafty_exc.NotAllowed as e:
        embed.color=0xff00ff
        embed.add_field(name='{} {}'.format(action, metadata['server_name']),
                        value='ERROR: I am legit not sure what this even means. Not allowed: {}'.format(e))
    if status==True:
        if action == 'query':
            embed.add_field(name='{} {}'.format(action, metadata['server_name']),
                            value='Running:\t{}\nCrashed:\t{}\nAutostart:\t{}'.format(
                                    stats['running'],stats['crashed'],metadata['auto_start']))
            if not stats['running']:
                embed.color = 0xff0000
        else:
            embed.add_field(name='{} {}'.format(action, metadata['server_name']),
                            value='Action successful!')
    return embed

@bot.event
async def on_ready():
    print('Logged in as {0} and ready to start crafting!'.format(bot.user))


def verify_channel(ctx):
    if ctx.message.channel.id in config['channel_binds']:
        return True
    else:
        return False

@bot.command()
@commands.check(verify_channel)
async def list(ctx):
    sv_list = cweb.list_mc_servers()
    table = []
    for s in sv_list:
        stats = cweb.get_server_stats(s['server_id'])
        table_row = {}
        table_row['id'] = s['server_id']
        table_row['name'] = s['server_name']
        for k in config['server_list_fields']:
            try:
                table_row[k] = stats[k]
            except KeyError:
                try:
                    table_row[k] = s[k]
                except KeyError:
                    sys.stderr.write(f"ERROR: Could not locate a value for key {k} when listing server {s['server_id']}")
        table.append(table_row)
    embed = make_embed("Server list")
    embed.add_field(name='Servers:', value='```{}```'.format(
        tabulate.tabulate(table, headers="keys")
    ))
    await ctx.send(embed=embed)

@bot.command()
@commands.check(verify_channel)
async def server_stats(ctx, *args):
    servers_to_list = []
    if len(args) <= 0:
        for s in cweb.list_mc_servers():
            servers_to_list.append(s['server_id'])
    else:
        for s in args:
            servers_to_list.append(int(s))
    print(args, servers_to_list)
    for s in servers_to_list:
        stats = cweb.get_server_stats(s)
        table = []
        for k in config['server_stat_fields']:
            if k == '_online_max':
                table.append(['players', f"{stats['online']}/{stats['max']}"])
            else:
                table.append([k, str(stats[k])])
        embed = make_embed("Server stats")
        embed.add_field(name=stats['server_id']['server_name'], inline=False, value='```{}```'.format(
            tabulate.tabulate(table)
        ))
        await ctx.send(embed=embed)

@bot.command()
@commands.check(verify_channel)
async def host_stats(ctx):
    stats = cweb.get_host_stats()
    embed = make_embed('Host stats:')
    embed.add_field(name='CPU:', inline=False, value='{p}% of {c} cores @ {s} GHz (max {m} GHz)'.format(
        p=stats['cpu_usage'],
        c=stats['cpu_cores'],
        s=stats['cpu_cur_freq']/1000.0,
        m=stats['cpu_max_freq']/1000.0
    ))
    embed.add_field(name='Memory:', inline=False, value='{p}%: {u} / {t}'.format(
        p=stats['mem_percent'],
        u=stats['mem_usage'],
        t=stats['mem_total']
    ))
    embed.add_field(name='Disk:', inline=False, value='{p}%: {u} / {t}'.format(
        p=stats['disk_percent'],
        u=stats['disk_usage'],
        t=stats['disk_total']
    ))
    embed.set_footer(text='ID:{}'.format(stats['id']))
    await ctx.send(embed=embed)

@bot.command()
@commands.check(verify_channel)
async def query(ctx, arg):
    e = crafty_control("query", arg)
    await ctx.send(embed=e)

@bot.command()
@commands.check(verify_channel)
async def start(ctx, arg):
    e = crafty_control("start", arg)
    await ctx.send(embed=e)


@bot.command()
@commands.check(verify_channel)
async def stop(ctx, arg):
    e = crafty_control("stop", arg)
    await ctx.send(embed=e)


@bot.command()
@commands.check(verify_channel)
async def restart(ctx, arg):
    e = crafty_control("restart", arg)
    await ctx.send(embed=e)

@bot.command()
@commands.check(verify_channel)
async def command(ctx, sv_id, command):
    e = crafty_control("command", sv_id, command)
    await ctx.send(embed=e)

@bot.command()
@commands.check(verify_channel)
async def backup(ctx, arg):
    e = crafty_control("backup", arg)
    await ctx.send(embed=e)

bot.run(config['discord_token'])

