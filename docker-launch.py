import json
import os

def write_config_file():
    """Writes the config.json file from the environment variables."""
    with open('config.json', 'w') as f:
        # Convert the VERIFY_SSL string to a boolean
        verify_ssl = os.environ['VERIFY_SSL'].lower() == 'true'

        data = {
            'discord_token': os.environ['DISCORD_TOKEN'],
            'crafty_url': os.environ['CRAFTY_URL'],
            'verify_ssl': verify_ssl,
            'crafty_token': os.environ['CRAFTY_TOKEN'],
            'channel_binds': [int(bind) for bind in os.environ['CHANNEL_BINDS'].split(',')],
        }
        json.dump(data, f, indent=2)

def main():
    """Checks for the config.json file and writes it if it does not exist."""
    if not os.path.exists('config.json'):
        write_config_file()
        os.system('python craftbot.py')
    else:
        print('The config.json file already exists.')
        os.system('python craftbot.py')

if __name__ == '__main__':
    main()
