FROM python:3.10-slim

#Define arguments

ARG discord_token
ARG crafty_url
ARG verify_ssl
ARG crafty_token
ARG channel_binds
LABEL containerizedby="Terrariadlc"

#Define ENVs

ENV DISCORD_TOKEN $discord_token
ENV CRAFTY_URL $crafty_url
ENV VERIFY_SSL $verify_ssl
ENV CRAFTY_TOKEN $crafty_token
ENV CHANNEL_BINDS $channel_binds

#Download git

RUN apt-get update && apt-get install git -y

#Copy cloned app
COPY requirements.txt .
RUN pip install -r requirements.txt
RUN mkdir /app
WORKDIR /app
COPY config.default.json .
COPY craftbot.py .
COPY docker-launch.py .

CMD ["python", "docker-launch.py"]
