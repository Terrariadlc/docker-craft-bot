# CraftBot

Simple discord.py bot for sending commands to the Crafty Controller
API.

See [their repo](https://gitlab.com/crafty-controller/crafty-web) for more info
about Crafty

## Commands

| Command     | Description                                            |
|-------------|--------------------------------------------------------|
| `list`      | Lists all servers by ID and shows their running status |
| `start n`   | Starts server N, where N is a number                   |
| `stop N`    | Stops server N, see start                              |
| `restart N` | Retarts server N, see start                            |

## Installation

I'd recommend setting up a virtualenv for this to run in so you don't have
to install to the system prefix, but it's technically optional

Prerequisites:

| Requirement     | Version  | Explanation                                        |
|-----------------|----------|----------------------------------------------------|
| `discord.py`    | >= 1.2.5 | for connecting to discord                          |
| `jsmin`         |          | for supporting comments in json                    |
| `crafty-client` |          | the star of the show :) / for connecting to Crafty |

1. Make the directory/user you want to run CraftBot in (I use one near where I
   run Crafty `/var/opt/minecraft/craftbot`)
   - `mkdir /var/opt/minecraft/craftbot`
2. Go there
   - `cd /var/opt/minecraft/craftbot`
3. Get the code
   - `git clone https://gitlab.com/computergeek125/CraftBot.git`
3. (optional, but recommended) Set up virtualenv
   - `python3 -m venv craftbot-venv`
   - `source ./craftbot-venv/bin/activate`
4. Install prerequisites
   - `python3 -m pip install -U -r requirements.txt`

## Configuration

To copy an example config, `cp config.default.json config.json` from the Git repo

There are four parameters in `config.json`:

 - `discord_token`: API key for connecting to Discord. See
   [Discord's developer page](https://discord.com/developers) for more info.
 - `crafty_url`: URL that the crafty client will connect to.  Typically, 
   `https://localhost:8000` is a sane default for most systems where CraftBot
   is running on the same server as Crafty, but you may have changed the port
   number, or may be running CraftBot on a different server
 - `crafty_token`: API key for connecting to Crafty.  This can be seen and re-
   generated on Crafty's Config page
 - `channel_binds`: The integer ID of channels where the bot can accept commands.
   In the Discord app, enable Developer Mode and right-click a channel, then "Copy
   ID". `channel_binds` is a list, but will run with a list length of 1 for only
   one channel

## Running

The bot's first and only parameter is the filename for the config file, defaulting to `config.json`.

Option 1: default config file location:

 - `cd /var/opt/minecraft/craftbot/CraftBot`
 - `python3 craftbot.py`

Option 2: setting config file location:

 - `cd /var/opt/minecraft/craftbot/CraftBot`
 - `python3 craftbot.py my_config_file.json`
