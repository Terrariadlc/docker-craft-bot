## This is a quick setup of craftbot

---

**Overview:**
1. [Enviroment variables](#env-var) 
2. [Prebuilt images](#prebuilt-images)
3. [How to build an image](#building-images)
##### For craftbot settings, read documentation at [Craftbot Overview](https://gitlab.com/computergeek125/CraftBot/-/issues)

---
### Enviroment variables <a name="env-var"></a>

**If you want to use custom config.json (for example to change the prefix) you will need to add a mount**

**To add one use, mount path:/app**
For docker run add `  -v {$PWD}/CraftBot:/app/ \`
For docker-compose add 
```
volumes:
      - ./CraftBot:/app/
```

**You will need to change https://localhost:8443 to a local ip (eg. https://192.168.1.30:8443) unless CraftBot is in the same network as Crafty**

---
### Prebuilt images <a name="prebuilt-images"></a>
**Using Docker run**

```d
docker run -d \
  -e DISCORD_TOKEN= \
  -e CRAFTY_URL=https://localhost:8443 \
  -e VERIFY_SSL= \
  -e CRAFTY_TOKEN= \
  -e CHANNEL_BINDS= \
  registry.gitlab.com/terrariadlc/docker-craft-bot:latest
```

**If you want to use docker-compose**

```d
version: '3'
services:
  craftbot:
    image: registry.gitlab.com/terrariadlc/docker-craft-bot:latest
    container_name: craftbot
    environment:
      - DISCORD_TOKEN=
      - CRAFTY_URL=https://localhost:8443
      - VERIFY_SSL=
      - CRAFTY_TOKEN=
      - CHANNEL_BINDS=
```

To start the image with compose, run this `docker-compose up -d`

---
### Building the image <a name="building-images"></a>

If you want to build the image locally, clone this gitlab repository and `cd` into it. Then you can run this to build your image
```d
docker build -t crafty_bot .
```
If you want to build the development branch, please use this instead
```d
docker build -f Dockerfile-DEV -t crafty_bot .
```

---